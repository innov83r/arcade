package ui;

import java.awt.Color;
import javax.swing.JButton;
import logic.SnakeLogic;
import logic.SnakeLogic.Status;
import logic.SnakeLogic.Type;

@SuppressWarnings("serial")
public class SnakeCell extends JButton
{
	JButton cell[][];
	int row;
	int col;
	Color color;
	SnakeLogic.Status status;
	SnakeLogic.Type type;
	
	public SnakeCell()
	{
		
	}
	
	public SnakeCell(int row, int col, Status status, Type type) 
	{
		
		this.row = row;
		this.col = col;
		this.status = status;
		this.type = type;
	}

	public JButton[][] setInactiveSnakeCell(int _row, int _col, Status _status) 
	{
		// TODO Auto-generated constructor stub
		cell = new JButton[_row][_col];
		this.status = _status;
		setColor(cell[_row][_col]);
		cell[_row][_col].repaint();
		return cell;
	}

	public void setColor(JButton _cell) 
	{
		switch(status)
		{
		case ACTIVE:
			_cell.setBackground(Color.black);
			break;
		case INACTIVE:
			_cell.setBackground(Color.white);
			break;
		case FOOD:
			_cell.setBackground(Color.green);

			break;
		default:
			break;
		}
		_cell.setOpaque(true);
		_cell.setBorderPainted(false);
	}
}
