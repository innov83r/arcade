package ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import logic.SnakeLogic;
import logic.SnakeLogic.Direction;
import logic.SnakeLogic.Status;

import javax.swing.JButton;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class SnakeGameBoard extends JFrame
{
	private static int gridSize;
	private static Status status;
	private static JFrame board;
	private static JButton cell[][];
	private static List<int[][]> snake = new ArrayList<int[][]>();
	
	public static void addSnakeBody(int row, int col) {
		int[][] position = new int[][] {{row}, {col}};
		snake.add(position);
	}

    public static List<int[][]> getSnake() {
		return snake;
	}

    private static void setCellStatus(Status _status)
    {
    	status = _status;
    }
    
    public static Status getCellStatus()
    {
    	return status;
    }

	private static void setGridSize(int size)
	{
		gridSize = size;
	}
	
	public static int getGridSize()
	{
		return gridSize;
	}
	
	public static void setSnakeHead(int row, int col)
	{
		cell[row][col].setBackground(Color.black);
		setCellStatus(Status.ACTIVE);
	}
	
	private int[] getSnakePosition() //testing for movement. may not need.
	{
		int[] position = new int[2];
		
		return position;
	}
	
	public static void setFood(int row, int col)
	{
		cell[row][col].setBackground(Color.green);
	}
	
	public static void moveSnake(Direction direction)
	{
		//int row, col;
		
		switch(direction)
		{
			case UP:
				for (int i = 0, len = getSnake().size(); i < len; i++)
				{
					int row = getSnake().get(i)[0][0];
					//row = row == gridSize ? 0 : row;
					int col = getSnake().get(i)[1][0];
					if (row > 0 || row < gridSize)
					{	
						cell[row][col].setBackground(Color.white);
						cell[(row - 1)][col].setBackground(Color.blue);
						//cell[row][col].repaint();
						cell[(row - 1)][col].repaint();
					}
				}
				break;
		default:
			break;
		}	
	}
	
	public static void initializeBoard(int gridDimension)
	{
		setGridSize(gridDimension);
		gridSize = getGridSize();
		//JButton[][] cell;
		cell = new JButton[gridSize][gridSize];
		
		JFrame.setDefaultLookAndFeelDecorated(true);
		board = new JFrame("Snake");
		
		board.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		board.setLayout(new GridLayout(gridSize, gridSize, 2, 2));
		board.setPreferredSize(new Dimension(500,500));
		for(int row = 0; row < gridSize; row++)
		{
			for(int col = 0; col < gridSize; col++)
			{
				SnakeCell sc = new SnakeCell();
				cell = sc.setInactiveSnakeCell(row, col, status.INACTIVE);
				
//				cell[row][col] = new JButton();
//				cell[row][col].setMinimumSize(new Dimension(1,1));
//				cell[row][col].setMaximumSize(new Dimension(1,1));
//				cell[row][col].setBackground(Color.white);
//				cell[row][col].setOpaque(true);
//				cell[row][col].setBorderPainted(false);
				
				board.add(cell[row][col]);

			}
		}
		board.pack();
		board.setVisible(true);
		
		int[] coords = SnakeLogic.getRandomNumbers(getGridSize(), true);
		setSnakeHead(coords[0], coords[1]);
		setFood(coords[2], coords[3]);
		addSnakeBody(coords[0], coords[1]);
		for(int i = 0, len = snake.size(); i < len; i++)
		{
			System.out.println(snake.get(i)[0][0]);
			System.out.println(snake.get(i)[1][0]);
		}
	}
	

}
