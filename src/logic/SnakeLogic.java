package logic;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.Timer;

import ui.*;

@SuppressWarnings("serial")
public class SnakeLogic extends SnakeGameBoard
{
	public enum Type 
	{
		BODY, HEAD
	}

	
	private static Timer timer;
	public enum Direction 
	{
		UP, DOWN, LEFT, RIGHT
	}
	public enum Status
	{
		ACTIVE, INACTIVE, FOOD
	}
	
	public static int[] getRandomNumbers(int max, boolean init)
	{
		int[] positions;
		Random rand = new Random();
		if (init)
		{
			positions = new int[4];
			for(int i = 0; i < 4; i++)
			{
				positions[i] = rand.nextInt(max);
			}
			return positions;
		}
		else
		{
			positions = new int[2];
			for(int i = 0; i < 2; i++)
			{
				positions[i] = rand.nextInt(max);
			}
			return positions;
		}
	}
	
	public static void main(String[] args) 
	{	
		//int[] coords = getRandomNumbers(getGridSize(), false);
		initializeBoard(25);
		timer = new Timer(100, new TimerListener());
		timer.start();
		//setNewSnakeBody(coords[0], coords[1]);
	}
	
	private static class TimerListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event) 
		{
			moveSnake(Direction.UP);
		}
	}
}
